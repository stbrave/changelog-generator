<?php

class JiraHelper {

    /**
     * Jira username
     *
     * @var string
     */
    private $username = null;

    /**
     * Jira API key
     *
     * @var string
     */
    private $apikey = null;

    /**
     * Jira domain
     *
     * @var string
     */
    private $domain = null;

    /**
     * Folder which contains cache for these methods
     *
     * @var string
     */
    private $cachefolder = null;

    /**
     * Creates a new JiraHelper
     *
     * The credentials can be found at https://id.atlassian.com/profile/profile.action
     *
     * @param string $username    The username
     * @param string $apikey      The API key
     * @param string $domain      The domain of Jira (ex.: mv-jira-1.atlassian.net)
     * @param string $cachefolder Folder which contains cache for these methods
     */
    public function __construct($username, $apikey, $domain, $cachefolder = __DIR__.'/jiracache')
    {
        $this->username = $username;
        $this->apikey = $apikey;
        $this->domain = $domain;
        $this->cachefolder = rtrim($cachefolder, '/\\');

        if (!is_dir($this->cachefolder)) {
            mkdir($this->cachefolder, 0777, true);
        }
    }

    /**
     * Defines the name of the cache file
     *
     * @param string $functionName Name of the function for which we want the filename (__FUNCTION__)
     * @param array  $params       A list of all parameters (func_get_args())
     *
     * @return string Full path to the cache file
     */
    private function getCacheFilename($functionName, array $params)
    {
        if (count($params) === 1 && preg_match('/^[a-z0-9\-_]{1,}$/i', $params[0]) === 1) {
            return $this->cachefolder.'/'.$functionName.'_'.$params[0];
        }
        return $this->cachefolder.'/'.$functionName.'_'.md5(serialize($params));
    }

    /**
     * Retrieves data from the cache, or returns false when there is no cache
     *
     * @param string $functionName Name of the function for which we retrieve data (__FUNCTION__)
     * @param array  $params       A list of all parameters (func_get_args())
     *
     * @return bool|mixed False when there is no data, otherwise the cached value
     */
    private function getCache($functionName, array $params)
    {
        $filename = $this->getCacheFilename($functionName, $params);
        if (file_exists($filename)) {
            return unserialize(file_get_contents($filename));
        }

        return false;
    }

    /**
     * Stores data into the cache
     *
     * @param string $functionName Name of the function for which we set data (__FUNCTION__)
     * @param array  $params       A list of all parameters (func_get_args())
     * @param mixed  $value        Value to store
     *
     * @return mixed Returns the value, so you can do "return $this->setCache(__FUNCTION__, func_get_args(), $return);"
     */
    private function setCache($functionName, array $params, $value)
    {
        $filename = $this->getCacheFilename($functionName, $params);
        file_put_contents($filename, serialize($value));

        return $value;
    }

    /**
     * Returns all data for an issue, null when the issue can't be found.
     *
     * @param string $issueIdOrKey The issue ID or issue key
     *
     * @return array|null
     *
     * @throws Exception An exception is thrown if Jira doesn't return valid data
     */
    public function getIssue($issueIdOrKey)
    {
        if (($return = $this->getCache(__FUNCTION__, func_get_args())) !== false) {
            return $return;
        }

        $url = '/rest/api/2/issue/'.rawurlencode($issueIdOrKey);
        $result = $this->curlGet($url);
        if ($result['http_code'] === 404) {
            return $this->setCache(__FUNCTION__, func_get_args(), null);
        }
        if ($result['http_code'] !== 200) {
            throw new \Exception("Invalid HTTP code ".$result['http_code']." for fetching issue ".$issueIdOrKey);
        }

        return $this->setCache(__FUNCTION__, func_get_args(), json_decode($result['content'], true));
    }

    /**
     * Performs a GET request on the API
     *
     * @param string $url URL to be requested; domain will be prefixed.
     *
     * @return array
     */
    private function curlGet($url)
    {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_HTTPHEADER     => array('Accept: application/json'),
            CURLOPT_HTTPGET        => true,
            CURLOPT_URL            => 'https://'.$this->domain.'/'.ltrim($url, '/'),
            CURLOPT_USERPWD        => $this->username.":".$this->apikey,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HEADER         => true,
            CURLOPT_RETURNTRANSFER => true,
        ]);

        $result = curl_exec($ch);
        $info   = curl_getinfo($ch);
        list($headers, $content) = explode("\r\n\r\n", $result, 2);

        curl_close($ch);

        return array_merge([
            'headers' => explode("\r\n", $headers),
            'content' => $content,
        ], $info);
    }
}