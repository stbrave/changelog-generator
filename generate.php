<?php

/**
 * @TODO
 * Timestamp anders formatten
 * Major versions anchoren
 * Icons vooraan zetten zonder tekst
 * Daaropvolgend een legenda voor icons
 */

require 'config.php';
require 'GitHelper.php';
require 'JiraHelper.php';

// Initiate helpers
$gitHelper = new GitHelper();
$jiraHelper = new JiraHelper($jira_username, $jira_apitoken, $jira_domain);

// Fetch data from GIT
$commits  = $gitHelper->getCommits();
$versions = $gitHelper->getTags();


foreach ($commits as $hash => $commit) {
    if (!isset($commit['hash'])) {
        continue;
    }

    // Add Bitbucket link and summary
    $commits[$hash]['url'] = $bitbucket_url . '/commits/' . $hash;
    $commits[$hash]['summary'] = $commit['subject'];

    // Add pullrequest data where possible
    $matches = array();
    if (preg_match('/\(pull request #([0-9]{1,})\)/', $commit['subject'], $matches) === 1) {
        $lines = explode("\n", $commit['body']);

        $commits[$hash]['summary'] = $lines[0];
        $commits[$hash]['pullrequest'] = [
            'url' => $bitbucket_url . '/pull-requests/' . $matches[1],
            'key' => $matches[1],
            'summary' => $lines[0],
        ];
    }

    // Add JIRA data for each commit where possible
    $matches = array();
    preg_match('/([A-Z]{2,10}-[0-9]{1,})/', $commit['subject'].PHP_EOL.$commit['body'], $matches);
    if (count($matches) && ($issue = $jiraHelper->getIssue($matches[1])) !== null) {
        $commits[$hash]['summary'] = $issue['fields']['summary'];
        $commits[$hash]['jira'] = [
            'url'     => 'https://' . $jira_domain . '/browse/' . $issue['key'],
            'key'     => $issue['key'],
            'summary' => $issue['fields']['summary'],
        ];
    }
}

// Groups commits by version
$grouped = array();
$version = null;
foreach ($commits as $hash => $data) {
    if (isset($versions[$hash]) && isset($versions[$hash]['tagstr'])) {
        $version = str_replace('refs/tags/', '', $versions[$hash]['tagstr']);
        $grouped[$version] = [];
    }
    if (isset($grouped[$version])) {
        array_push($grouped[$version], $data);
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Changelog</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="http://cdn.redics.net/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="http://cdn.redics.net/font-awesome/5.0.13/css/fontawesome-all.css">
        <style type="text/css">
            ul {
                margin-top: 0;
                margin-bottom: 0;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Dokteronline Changelog</h1>
                </div>
                <div class="col-sm-12 col-lg-3">
                    <form class="form">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="show" id="show_major" value="major">
                            <label class="form-check-label" for="show_major">Show major versions</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="show" id="show_minor" value="minor" checked>
                            <label class="form-check-label" for="show_minor">Show minor versions</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="show" id="show_patch" value="patch">
                            <label class="form-check-label" for="show_patch">Show all versions</label>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12 col-lg-9">
<?php

// Shows them by version
$last_major = $major = null;
$last_minor = $minor = null;
$patch = null;
foreach ($grouped as $version => $data) {
    if (!isset($data[0]) || !isset($data[0]['date'])) {
        continue;
    }

    // Store Jira tickets in here to prevent duplicates
    $jiraDeDuplicator = [];
    // Store summaries as well to prevent duplicates
    $summaryDeDuplicator = [];

    // Fetches major, minor and patch stuff
    if (substr_count($version, '.') == 2) {
        list($major, $minor, $patch) = explode('.', $version);
    }

    // Headers
    if ($major && $major != $last_major) {
        $last_major = $major;
        echo '<h3 class="major">'.$major.' <small>'.$data[0]['date'].'</small></h3>'.PHP_EOL;
    }
    if ($minor && $minor != $last_minor) {
        $last_minor = $minor;
        echo '<h3 class="minor">'.$major.'.'.$minor.' <small>'.$data[0]['date'].'</small></h3>'.PHP_EOL;
    }
    echo '<h3 class="patch">'.$version.' <small>'.$data[0]['date'].'</small></h3>'.PHP_EOL;
    echo '<ul>'.PHP_EOL;
    foreach($data as $commit) {
        if (!isset($commit['hash'])) {
            continue;
        }

        // Prevent duplicate based on summary (this happened a few times)
        if (in_array($commit['summary'], $summaryDeDuplicator)) {
            continue;
        }
        array_push($summaryDeDuplicator, $commit['summary']);

        // Prevent duplicate based on Jira ticket (this often happened)
        if (isset($commit['jira'])) {
            if (in_array($commit['jira']['key'], $jiraDeDuplicator)) {
                continue;
            }
            array_push($jiraDeDuplicator, $commit['jira']['key']);
        }

        // Output HTML
        echo "\t<li><span>" . ucfirst(htmlspecialchars($commit['summary']));
        echo '</span> [<a href="'.$commit['url'].'" alt="Commit @ Bitbucket" target="_blank"><i class="fab fa-bitbucket"></i> commit</a>';
        if (isset($commit['pullrequest'])) {
            echo ' | <a href="' . $commit['pullrequest']['url'] . '" alt="Pullrequest @ Bitbucket" target="_blank"><i class="far fa-check-square"></i> pullrequest</a>';
        }
        if (isset($commit['jira'])) {
            echo ' | <a href="' . $commit['jira']['url'] . '" alt="Issue @ Jira" target="_blank"><i class="fas fa-ticket-alt"></i> ticket</a>';
        }
        echo ']</li>'.PHP_EOL;
    }
    echo '</ul>' . PHP_EOL;
}
?>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="http://cdn.redics.net/jquery/1.11.2/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="http://cdn.redics.net/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            function toggle(value) {
                $('.patch').hide();
                $('.minor').hide();
                $('.major').hide();
                if(value == 'major') {
                    $('.major').show();
                }
                else if(value == 'minor') {
                    $('.minor').show();
                }
                else if(value == 'patch') {
                    $('.patch').show();
                }
            }

            $(function() {
                // Toggle headers
                $('[name=show]').change(function() {
                    toggle($('[name=show]:checked').val());
                });

                // Default value
                toggle('minor');
            });
        </script>
    </body>
</html>
