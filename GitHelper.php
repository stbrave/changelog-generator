<?php

class GitHelper {
    /**
     * Formats text into a multidimensional array.
     *
     * @param string $text          Input text.
     * @param array  $labels        Labels for the multi dimensional array; if not specified, numeric keys are used.
     * @param string $col_separator Col separator, by default a space.
     * @param string $row_separator Row separator, by default a new line.
     * @param bool   $first_as_key  When true, the first column value will be used as key in the array.
     *
     * @return array
     */
    private static function recursiveExplode(
        $text,
        $labels = [],
        $col_separator = " ",
        $row_separator = "\n",
        $first_as_key = true
    ) {
        $rows = explode($row_separator, $text);
        $ret = array();
        foreach ($rows as $line) {
            $cols = explode($col_separator, $line);
            if ($first_as_key) {
                $ret[trim($cols[0])] = count($cols) === count($labels) ? array_combine($labels, $cols) : $cols;
            } else {
                $ret[] = count($cols) === count($labels) ? array_combine($labels, $cols) : $cols;
            }
        }
        return $ret;
    }

    /**
     * Returns all commits in a branch.
     *
     * @param string $branch Name of the branch, default: master.
     *
     * @return array A multidimensional array with keys 'hash', 'author', 'date', 'subject' and 'body'
     */
    public function getCommits($branch = 'master')
    {
        return static::recursiveExplode(
            `git log ${branch} --pretty=format:"%H%x0e%an%x0e%aD%x0e%s%x0e%b%x0d"`,
            ['hash', 'author', 'date', 'subject', 'body'],
            chr(0x0e),
            chr(0x0d)
        );
    }

    /**
     * Returns all tags.
     *
     * @return array A multidimensional array with keys 'hash', 'tag' and 'tagstr'
     */
    public function getTags()
    {
        $return = static::recursiveExplode(`git show-ref --tags --dereference`, ['hash', 'tag']);

        // Remove dereferenced suffixes
        foreach ($return as &$data) {
            if (!isset($data['tag'])) {
                continue;
            }
            $data['tagstr'] = str_replace('^{}', '', $data['tag']);
        }

        return $return;
    }
}